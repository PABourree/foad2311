<?php

require_once "inc/class-eleve"

if (!empty($_POST)) {

    if (!empty($_POST)){
        $eleve = new eleve();
        $eleve->nom = $_POST['nom'];
        $eleve->prenom = $_POST['prenom'];
        $eleve->datenais = $_POST['datenais'];
        $eleve->moyenne = $_POST['moyenne'];
        $eleve->appreciation = $_POST['appreciation'];
        $eleve->classeid = $_POST['classeid'];
        $eleve->eleveid = $_GET['eleveid'];
        $eleve->modifiereleve();
    }
}

?>

<!DOCTYPE html>
<html>
<head>
<script src="lib/jquery-3.4.1.min.js"></script>
    <script src="lib/jquery.validate.min.js"></script>
    <script>
        $(function(){
            $("form").validate();
            rules: {
                nom: "required",
                prenom: "required",
                datenais: "required",
                moyenne: "required",
                appreciation: "required",
                classeid: "required"
            }
            messages: {
                nom: "Nom non renseigné",
                prenom: "Prenom non renseigné",
                datenais: "date de naissance non renseignée",
                moyenne: "Moyenne non renseignée",
                appreciation: "Appreciation non renseignée",
                classeid: "ID de la classe non renseignée"  
            }
        }
        </script>
</head>
<body>
    <a>Modifier l'éleve</a>
<form method="POST">
        <input type="text" id="nom" name="nom" placeholder="nom">
        <input type="text" id="prenom" name="prenom" placeholder="prenom">
        <input type="text" id="datenais"  name="datenais" placeholder="datenais">
        <input type="text" id="moyenne"  name="moyenne" placeholder="moyenne">
        <input type="text" id="appreciation" name="appreciation" placeholder="appreciation">
        <input type="text" id="classeid"  name="classeid" placeholder="classeid">
        <input type="submit" />
    </form>
</body>
</html>
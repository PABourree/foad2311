<?php

require_once "inc/class-classe.php";
require_once "inc/class-eleve.php"

if (!empty($_POST)){
    $eleve = new eleve();
    $eleve->nom = $_POST['nom'];
    $eleve->prenom = $_POST['prenom'];
    $eleve->datenais = $_POST['datenais'];
    $eleve->moyenne = $_POST['moyenne'];
    $eleve->appreciation = $_POST['appreciation'];
    $eleve->classeid = $_POST['classeid'];
    $eleve->creereleve();
}

$classe = new Classe;
$classe->getEleves();

?>
<!DOCTYPE html>
<html>
<head>
<script src="lib/jquery-3.4.1.min.js"></script>
    <script src="lib/jquery.validate.min.js"></script>
    <script>
        $(function(){
            $("form").validate();
            rules: {
                nom: "required",
                prenom: "required",
                datenais: "required",
                moyenne: "required",
                appreciation: "required",
                classeid: "required"
            }
            messages: {
                nom: "Nom non renseigné",
                prenom: "Prenom non renseigné",
                datenais: "date de naissance non renseignée",
                moyenne: "Moyenne non renseignée",
                appreciation: "Appreciation non renseignée",
                classeid: "ID de la classe non renseignée"  
            }
        }
        </script>

</head>
<body>
    <form method="POST">
        <input type="text" name="nom" placeholder="nom">
        <input type="text" name="prenom" placeholder="prenom">
        <input type="text" name="datenais" placeholder="datenais">
        <input type="text" name="moyenne" placeholder="moyenne">
        <input type="text" name="appreciation" placeholder="appreciation">
        <input type="text" name="classeid" placeholder="classeid">
    </form>
<div>
    <h2><a><?= $classe->eleves[$i]->nom ?><?= $classe->eleves[$i]->prenom ?><?= $classe->classe[$i]->classenombre ?><?= $classe->classe[$i]->classemoyenne ?></a></h2>
    <a href="liste.php">Voir et modifier les éléves</a>
</div>

</body>
</html>
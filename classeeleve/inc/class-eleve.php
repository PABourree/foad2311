<?php

require_once "inc/class-singleton.php";

class Eleve {
    public $eleveid;
    public $nom;
    public $prenom;
    public $datenais;
    public $moyenne;
    public $appreciation;
    public $classeid;

    function __construct() {
        $this->eleveid = 0;
        $this->nom = "";
        $this->prenom = "";
        $this->datenais = "";
        $this->moyenne = 0;
        $this->appreciation = "";
        $this->classeid = 0;
    }

    public static function construct_load($eleveid) {
		$eleve = new Eleve();
		$eleve->load($eleveid);
		return $eleve;
    }
    
    public function load($id) {
		$db_connect = db_connect::construit("localhost", "root", "", "baseeleve");
		
		//On charge le message
		$requete = "SELECT nom, prenom, datenais, moyenne, appreciation, classeid FROM eleve WHERE eleveid=:eleveid";
		$stmt = $db_connect->connexion->prepare($requete);
		$stmt->execute(array(":eleveid"=>$eleveid));
		$resultat = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->eleveid = $eleveid;
		$this->nom = $resultat['nom'];
		$this->prenom = $resultat['prenom'];
		$this->datenais = $resultat['datenais'];
		$this->moyenne = $resultat['moyenne'];
        $this->appreciation = $resultat['appreciation'];
        $this->classeid = $resultat['classeid'];
    }
    public function creereleve() {
        $db_connect = db_connect::construit("localhost", "root", "", "baseeleve");

        $requete = "INSERT INTO eleve (nom, prenom, datenais, moyenne, appreciation, classeid) VALUES (:nom, :prenom, :datenais, :moyenne, :appreciation, :classeid)";
        $stmt = $db_connect->connexion->prepare($requete);
        return $stmt->execute(array(
            ":nom"	=>	$this->nom,
            ":prenom"	=>	$this->prenom,
            ":datenais"	=>	$this->datenais,
            ":moyenne"	=>	$this->moyenne,
            ":appreciation"	=>	$this->appreciation,
            ":classeid"	=>	$this->classeid
        ));
    }
    public function modifiereleve() {
        $requete = "UPDATE eleve SET nom=:nom, prenom=:prenom, datenais=:datenais, moyenne=:moyenne, appreciation=:appreciation, classeid=:classeid WHERE eleveid=:eleveid";
        $stmt = $db_connect->connexion->prepare($requete);
        $stmt->bindParam(':nom', $this->nom, PDO::PARAM_STR);
        $stmt->bindParam(':prenom', $this->prenom, PDO::PARAM_STR);
        $stmt->bindParam(':datenais', $this->datenais, PDO::PARAM_STR);
        $stmt->bindParam(':moyenne', $this->moyenne, PDO::PARAM_STR);
        $stmt->bindParam(':appreciation', $this->appreciation, PDO::PARAM_STR);
        $stmt->bindParam(':classeid', $this->classeid, PDO::PARAM_STR
        $stmt->bindParam(':eleveid', $this->eleveid, PDO::PARAM_INT);
        return $stmt->execute();

    }

